FROM alpine:3.17.3 as builder
COPY builder/rootfs /
RUN builder.sh prepare
COPY --chown=abuild:abuild builder/abuild /home/abuild
USER abuild
RUN builder.sh build

FROM alpine:3.17.3 as copier
COPY --from=builder /home/abuild/.abuild/*.rsa.pub /etc/apk/keys/
COPY --from=builder /home/abuild/packages/abuild/ /packages/

ENV ICECAST_VERSION=2.4.4
ENV GOMPLATE_VERSION=3.9.0
ENV MAILCAP_VERSION=2.1.52-r0
ENV LIBXML2_UTILS_VERSION=2.9.12-r1

RUN set -eux; \
      echo '/packages' >> /etc/apk/repositories; \
      apk add --no-cache \
        "icecast=${ICECAST_VERSION}-r99" \
        "gomplate=${GOMPLATE_VERSION}-r99" \
        "mailcap=${MAILCAP_VERSION}" \
        "libxml2-utils=${LIBXML2_UTILS_VERSION}" \
      ; \
      rm -rf /packages;

FROM scratch

COPY --from=copier / /
COPY rootfs /

EXPOSE 8000/tcp

ENTRYPOINT [ "entrypoint" ]
CMD [ "icecast" ]
