FROM ubuntu:20.04

ENV TZ=UTC

ENV LIQUIDSOAP_VERSION=1.4.4
ENV CURL_VERSION=7.68.0-1ubuntu2.5
ENV CA_CERTIFICATES_VERSION=20210119~20.04.1
ENV GOSU_VERSION=1.10-1ubuntu0.20.04.1

ENV LIQUIDSOAP_RELEASE="https://github.com/savonet/liquidsoap/releases/download/v${LIQUIDSOAP_VERSION}"
ENV LIQUIDSOAP_DEB="liquidsoap-v${LIQUIDSOAP_VERSION}_${LIQUIDSOAP_VERSION}-ubuntu-focal-amd64-1_amd64.deb"
ENV LIQUIDSOAP_SHA512SUM="3d6411b9eb2e872855ae527b02ae347f7f463fc00a9c220019ea60bb09c73611d187b7abb16637f3d8bb2f91ec3813b49c837e93195cb5d6f319e2851e5091d5"

RUN set -eux; \
      apt-get update -y; \
      DEBIAN_FRONTEND=noninteractive \
      apt-get install -y --no-install-recommends \
        ca-certificates="${CA_CERTIFICATES_VERSION}" \
        curl="${CURL_VERSION}" \
        gosu="${GOSU_VERSION}" \
      ; \
      curl -L "${LIQUIDSOAP_RELEASE}/${LIQUIDSOAP_DEB}" --output "${LIQUIDSOAP_DEB}"; \
      echo "${LIQUIDSOAP_SHA512SUM}  ${LIQUIDSOAP_DEB}" >/liquidsoap.sha512; \
      sha512sum -c liquidsoap.sha512; \
      DEBIAN_FRONTEND=noninteractive \
      apt-get install -y --no-install-recommends \
        "/${LIQUIDSOAP_DEB}" \
      ; \
      apt-get autoremove --purge -y; \
      apt-get clean -y; \
      rm -rf \
        /var/lib/apt \
        /var/lib/cache \
        /var/lib/log \
        /liquidsoap.sha512 \
        "/${LIQUIDSOAP_DEB:?}" \
      ;

COPY rootfs /

ENTRYPOINT [ "entrypoint" ]
CMD [ "liquidsoap", "/var/liquidsoap/default.liq" ]
